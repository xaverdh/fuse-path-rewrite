{-# language LambdaCase, StandaloneDeriving #-}
module Main where

import System.Environment (getArgs,getProgName)
import System.FilePath ((</>))

import System.Fuse
import System.Posix.Files
import System.Posix.Directory
import System.Posix.Types
import System.Posix.IO

import qualified System.IO.Error as IOE
import System.IO.Error (tryIOError)

import Control.Concurrent
import Control.Applicative
import Control.Monad
import Control.Monad.Extra
import Control.Exception (onException,IOException)
import Data.Monoid
import Data.Function
import Data.Functor
import Data.Bifunctor
import Data.Maybe (fromMaybe,catMaybes)

import qualified Data.ByteString as BS
import qualified Data.ByteString.Internal as BSI

import Foreign.C.Error
import Foreign.Ptr (plusPtr)
import Foreign.ForeignPtr (withForeignPtr)
import GHC.IO.Device (SeekMode(..))

import Debug.Trace

deriving instance Show OpenFileFlags
deriving instance Show OpenMode

newtype DStream = DStream DirStream

data Hdl = Hdl Fd (MVar Bool)

hdlGetFD :: Hdl -> Fd
hdlGetFD (Hdl fd _) = fd

mkHdl :: Fd -> IO Hdl
mkHdl fd = Hdl fd <$> newMVar False

isClosedHdl :: Hdl -> IO Bool
isClosedHdl (Hdl _ mvar) = readMVar mvar

closeHdl :: Hdl -> IO ()
closeHdl (Hdl fd m) = do
  cl <- takeMVar m
  if cl
    then putMVar m cl
    else
      ( putMVar m True
       *> closeFd fd )
      `onException` putMVar m cl

main :: IO ()
main = do
  prog <- getProgName
  path:args <- getArgs
  let f p = pure $ path <> p
  fuseRun prog args (operations f) defaultExceptionHandler


operations :: (FilePath -> IO FilePath) -> FuseOperations Hdl DStream
operations tr = defaultFuseOps
  { fuseGetFileStat = getFileStat_ tr
  , fuseReadSymbolicLink = readSymbolicLink_ tr
  , fuseCreateDevice = createDevice_ tr
  , fuseCreateDirectory = createDirectory_ tr
  , fuseRemoveLink = removeLink_ tr
  , fuseRemoveDirectory = removeDirectory_ tr
  , fuseCreateSymbolicLink = createSymbolicLink_ tr
  , fuseRename = rename_ tr
  , fuseCreateLink = createLink_ tr
  , fuseSetFileMode = setFileMode_ tr
  , fuseSetOwnerAndGroup = setOwnerAndGroup_ tr
  , fuseSetFileSize = setFileSize_ tr
  , fuseSetFileTimes = setFileTimes_ tr
  , fuseOpen = open_ tr
  , fuseRead = read_ tr
  , fuseWrite = write_ tr
  -- , fuseGetFileSystemStats = 
  , fuseFlush = flush_ tr
  , fuseRelease = release_ tr
  -- , fuseSynchronizeFile =
  , fuseOpenDirectory = openDirectory_ tr
  , fuseReadDirectory = readDirectory_ tr
  , fuseReleaseDirectory = releaseDirectory_ tr
  -- , fuseSynchronizeDirectory = synchronizeDirectory_ tr
  -- , fuseAccess = 
  -- , fuseInit = 
  -- , fuseDestroy =
  }

(~>) :: (a -> Bool) -> b -> a -> Maybe b
(~>) f v a = if f a then Just v else Nothing

switch :: a -> [ (a -> Maybe b) ] -> b -> b
switch a cases deflt = 
  case catMaybes $ fmap ($a) cases of
    [] -> deflt
    val : _ -> val

ioErrToErrno :: IOException -> Errno
ioErrToErrno err = switch err
 [ IOE.isDoesNotExistError ~> eNOENT ]
 eFAULT

getFileStat_ tr p = do
  p' <- tr p
  tryIOError (statFileSym p') 
    <**> pure ( first ioErrToErrno )

statFileSym p = do
  st <- getSymbolicLinkStatus p
  pure $ FileStat
    { statEntryType = switch st
        [ isNamedPipe ~> NamedPipe
        , isCharacterDevice ~> CharacterSpecial
        , isDirectory ~> Directory
        , isBlockDevice ~> BlockSpecial
        , isRegularFile ~> RegularFile
        , isSymbolicLink ~> SymbolicLink
        , isSocket ~> Socket ]
        Unknown
    ,  statFileMode = fileMode st
    ,  statLinkCount = linkCount st
    ,  statFileOwner = fileOwner st
    ,  statFileGroup = fileGroup st
    ,  statSpecialDeviceID = specialDeviceID st
    ,  statFileSize = fileSize st
    ,  statBlocks = 0 -- XXX
    ,  statAccessTime = accessTime st
    ,  statModificationTime = modificationTime st
    ,  statStatusChangeTime = statusChangeTime st }

readSymbolicLink_ tr p = 
  tr p >>= fmap Right . readSymbolicLink

createDevice_ tr p ftype mode id = do
  p' <- tr p
  case ftype of
    NamedPipe -> createNamedPipe p' mode
    Directory -> createDirectory p' mode
    RegularFile -> createFile p' mode >>= closeFd
    _ -> createDevice p' mode id
  pure eOK

createDirectory_ tr p m = do
  p' <- tr p
  createDirectory p' m $> eOK

removeLink_ tr p = do
  p' <- tr p
  removeLink p' $> eOK

removeDirectory_ tr p = do
  p' <- tr p
  removeDirectory p' $> eOK

createSymbolicLink_ tr p1 p2 = do
  p1' <- tr p1
  p2' <- tr p2
  createSymbolicLink p1' p2' $> eOK

rename_ tr p1 p2 = do
  p1' <- tr p1
  p2' <- tr p2
  rename p1' p2' $> eOK

createLink_ tr p1 p2 = do
  p1' <- tr p1
  p2' <- tr p2
  createLink p1' p2' $> eOK

setFileMode_ tr p m = do
  p' <- tr p
  setFileMode p' m $> eOK

setOwnerAndGroup_ tr p u g = do
  p' <- tr p
  setOwnerAndGroup p' u g $> eOK

setFileSize_ tr p off = do
  p' <- tr p
  setFileSize p' off $> eOK

setFileTimes_ tr p t1 t2 = do
  p' <- tr p
  setFileTimes p' t1 t2 $> eOK

open_ tr p m flags = do
  p' <- tr p
  tryIOError (openFd p' m Nothing flags) >>= \case
    Right fd -> Right <$> mkHdl fd
    Left err -> pure . Left $ ioErrToErrno err


position h off = case fromIntegral off of
  0 -> pure ()
  n -> fdSeek (hdlGetFD h) AbsoluteSeek off $> ()

read_ tr _ h cnt off =
  ifM (isClosedHdl h) (pure $ Left eBADF) $ do
    position h off
    bs <- BSI.createUptoN (fromIntegral cnt) $ \buf ->
      fromIntegral <$> fdReadBuf (hdlGetFD h) buf cnt
    pure $ Right bs

write_ tr _ h bs off =
  ifM (isClosedHdl h) (pure $ Left eBADF) $ do
    position h off
    let (ptr,pos,len) = BSI.toForeignPtr bs
    fmap (Right . fromIntegral) $ withForeignPtr ptr $ \buf ->
      fdWriteBuf (hdlGetFD h) (buf `plusPtr` pos) (fromIntegral len)

flush_ tr p h = pure eOK -- XXX

release_ tr _ h = closeHdl h

openDirectory_ tr p = do
  p' <- tr p
  stream <- openDirStream p'
  pure . Right $ DStream stream

readDirectory_ tr p (DStream stream) = do
  p' <- tr p
  contents <- listAll p' stream []
  pure $ Right contents
  where
    listAll dir stream xs = do
      entry <- readDirStream stream
      if entry == ""
        then pure xs
        else do
          stat <- statFileSym (dir </> entry)
          listAll dir stream ((entry,stat): xs)

releaseDirectory_ tr _ (DStream stream) = do
  closeDirStream stream
  pure eOK

-- synchronizeDirectory_ tr (DStream stream) synctyp



