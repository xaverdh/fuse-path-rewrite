{ nixpkgs ? import <nixpkgs> {}, compiler ? "default", doBenchmark ? false }:

let

  inherit (nixpkgs) pkgs;

  hfuse-src = pkgs.fetchFromGitLab {
    repo = "hfuse";
    owner = "xaverdh";
    rev = "203e6fd313a26d10e429d01dfe948b7b1a116e5b";
    sha256 = "1a18gs7ydymzbkc7vwb3y0m8qpyly77qg3pjygjmhw3c8a0219cm";
  };
  hfuse = haskellPackages.callCabal2nix "HFuse" hfuse-src {};

  f = { mkDerivation, base, bytestring, filepath, stdenv, unix, extra }:
      mkDerivation {
        pname = "fuse-path-rewrite";
        version = "0.1.0.0";
        src = ./.;
        isLibrary = false;
        isExecutable = true;
        executableHaskellDepends = [
          base
          bytestring
          filepath
          unix
          extra
          hfuse
        ];
        description = "fuse filesystem for path rewriting";
        license = stdenv.lib.licenses.gpl3Plus;
      };

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;

  drv = variant (haskellPackages.callPackage f {});

in

  if pkgs.lib.inNixShell then drv.env else drv
